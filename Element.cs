﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Element
{
	public bool finished = false;
	public virtual void onAddedToQueue() {}
	public virtual void onActive() {}
	public virtual void update(float time_delta_fraction) {}
	public virtual void onRemove() {}

	public static void disruptElement(List<Element> ele_queue, Element ele)
	{
		if(ele_queue.Count > 0)
		{
			ele_queue[0].onRemove();
			ele_queue[0] = ele;
		}
		else
			ele_queue.Add(ele);
		
		ele_queue[0].onAddedToQueue();
		ele_queue[0].onActive();
	}
	
	public static void addElement(List<Element> ele_queue, Element ele)
	{
		ele_queue.Add(ele);
		if(ele_queue.Count == 1)
			ele.onActive();
	}

	public static void updateElements(List<Element> ele_queue)
	{
		Application.targetFrameRate = 60;
		if(ele_queue.Count > 0)
		{
			float time_delta_fraction = Time.deltaTime / (1.0f / Application.targetFrameRate);
			ele_queue[0].update(time_delta_fraction);
			
			if(ele_queue[0].finished)
			{
				ele_queue[0].onRemove();
				ele_queue.RemoveAt(0);
				if(ele_queue.Count > 0)
					ele_queue[0].onActive();
			}
		}
	}
}

public class ElementStandardMovement : Element
{
	Player player;
	public ElementStandardMovement (Player player)
	{
		this.player = player;
	}
	
	public override void update(float time_delta_fraction)
	{
		Vector3 movement = Vector3.zero;

		if(player.player_number == 1)
		{
			if(Input.GetKey(KeyCode.RightArrow))
				movement += new Vector3(player.running_speed, 0, 0);
			if(Input.GetKey(KeyCode.LeftArrow))
				movement += new Vector3(-player.running_speed, 0, 0);
			if(Input.GetKey(KeyCode.UpArrow))
				movement += new Vector3(0, 0, player.running_speed * 0.8f);
			if(Input.GetKey(KeyCode.DownArrow))
				movement += new Vector3(0, 0, -player.running_speed * 0.8f);
		}
		else if (player.player_number == 2)
		{
			if(Input.GetKey(KeyCode.D))
				movement += new Vector3(player.running_speed, 0, 0);
			if(Input.GetKey(KeyCode.A))
				movement += new Vector3(-player.running_speed, 0, 0);
			if(Input.GetKey(KeyCode.W))
				movement += new Vector3(0, 0, player.running_speed * 0.8f);
			if(Input.GetKey(KeyCode.S))
				movement += new Vector3(0, 0, -player.running_speed * 0.8f);
		}

		player.transform.position += movement * time_delta_fraction;
		
		if(movement.x > 0)
			player.transform.localScale = new Vector3(-1, 1, 1);
		if(movement.x < 0)
			player.transform.localScale = new Vector3(1, 1, 1);

		if(movement.magnitude > 0)
			player.GetComponent<Animator>().SetBool("is_moving", true);
		else
			player.GetComponent<Animator>().SetBool("is_moving", false);

		// Jump
		if(Input.GetKeyDown(KeyCode.Space))
			Element.disruptElement(player.ele_queue, new ElementJump(player, new Vector3(movement.x * 40, 20, movement.z)));
	}
}



public class ElementAddElement : Element
{
	List<Element> ElementQueue;
	Element ele;
	
	public ElementAddElement(List<Element> ElementQueue, Element ele) 
	{ 
		this.ElementQueue = ElementQueue;
		this.ele = ele;
	}
	
	public override void onActive()
	{
		Element.addElement(ElementQueue, ele);
		finished = true;
	}
}

public class ElementDisruptElement : Element
{
	List<Element> ElementQueue;
	Element ele;
	
	public ElementDisruptElement(List<Element> ElementQueue, Element ele) 
	{ 
		this.ElementQueue = ElementQueue;
		this.ele = ele;
	}
	
	public override void onActive()
	{
		Element.disruptElement(ElementQueue, ele);
		finished = true;
	}
}

public class ElementKo : Element
{
	Player player;

	public ElementKo(Player player) 
	{ 
		this.player = player;
	}
	
	public override void onActive()
	{
		player.GetComponent<Player>().current_state = Player.PlayerState.KO;
		player.GetComponent<Animator>().enabled = false;
		//player.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
		player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
		player.GetComponent<Rigidbody>().useGravity = true;
		player.GetComponent<Rigidbody>().angularDrag = 0.0f;
		player.GetComponent<Rigidbody>().angularVelocity = UnityEngine.Random.onUnitSphere;
		player.transform.Rotate(new Vector3(45, 0, 0));
		player.GetComponent<Renderer>().material.color = Color.red;
	}
	
	public override void update(float time_delta_fraction)
	{
		//life -= time_delta_fraction;
		//if(life <= 0)
			//finished = true;
	}
}



public class ElementJump : Element
{
	Player player;
	Vector3 tranjectory = Vector3.zero;

	public ElementJump(Player player, Vector3 trajectory) 
	{ 
		this.player = player;
		this.tranjectory = trajectory;
	}
	
	public override void onActive()
	{
		player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
		//player.GetComponent<ground_controller>().enabled = false;
		player.GetComponent<Rigidbody>().useGravity = true;
		player.GetComponent<Rigidbody>().velocity += tranjectory;
	}
	
	public override void update(float time_delta_fraction)
	{
		RaycastHit hit = new RaycastHit();
		if (Physics.Raycast(player.transform.position, -Vector3.up, out hit, 6)) {
			if(player.GetComponent<Rigidbody>().velocity.y <= 0)
				finished = true;
		}
		MonoBehaviour.print(player.GetComponent<Rigidbody>().velocity.y);

		if(Input.GetKeyUp(KeyCode.Space) && !finished && player.GetComponent<Rigidbody>().velocity.y > 0)
		{
			//print("hi");
			player.GetComponent<Rigidbody>().velocity = new Vector3(player.GetComponent<Rigidbody>().velocity.x,
			                                                        0, 
			                                                        player.GetComponent<Rigidbody>().velocity.z);
		}
	}
	
	public override void onRemove()
	{
		player.GetComponent<Rigidbody>().useGravity = false;
		//player.GetComponent<ground_controller>().enabled = true;
		player.GetComponent<Renderer>().material.color = Color.white;
		player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY;
	}
}

/*
 * This element is responsible for handling an entity in its "Stunned" state.
 */
public class ElementStunned : Element
{
	Entity entity;
	float life = 0.0f;
	bool shouldSpin;
	
	public ElementStunned(Entity entity, float stunTicks, bool shouldSpin)
	{ 
		this.entity = entity;
		life = stunTicks;
		this.shouldSpin = shouldSpin;
	}
	
	public override void onActive()
	{
		entity.currentState = EntityState.STUNNED;
		// Color the object Red to indicate damage.
		entity.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
		
		// If the stunned entity should spin, adjust constraints, and provide an angular velocity
		// for a fun physics effect.
		if(shouldSpin)
		{
			entity.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
			entity.GetComponent<Rigidbody>().angularVelocity = UnityEngine.Random.onUnitSphere * 10;
		}
	}
	
	public override void update(float time_delta_fraction)
	{
		life -= time_delta_fraction;
		
		// Setting 'finished' to true ends the Element.
		if(life <= 0)
			finished = true;
	}
	
	public override void onRemove()
	{
		entity.currentState = EntityState.NORMAL;
		
		// Return the entity's color to normal.
		entity.GetComponent<Renderer>().material.color = Color.white;
		
		// Reconfigure the entity's physics constraints.
		if(shouldSpin)
		{
			entity.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;
			entity.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
			
			Quaternion q = new Quaternion();
			q.eulerAngles = Vector3.zero;
			entity.transform.rotation = q;
		}
	}
}

public class ElementDodge : Element
{
	Player player;
	Vector3 dest;
	float life = 0;
	
	public ElementDodge(Player player, Vector3 dest, int stunTicks) 
	{ 
		this.player = player;
		life = stunTicks; 
		this.dest = dest;
	}
	
	public override void onActive()
	{
	}
	
	public override void update(float time_delta_fraction)
	{
		life -= time_delta_fraction;
		if(life <= 0)
			finished = true;

		player.GetComponent<Rigidbody>().velocity = (dest - player.transform.position) * 20f * time_delta_fraction;
	}
}


public class ElementNoop : Element
{
	float life = 0;
	
	public ElementNoop(float stunTicks)
	{ 
		life = stunTicks; 
	}
	
	public override void update(float time_delta_fraction)
	{
		life -= time_delta_fraction;
		if(life <= 0)
			finished = true;
	}
}

public class ElementMoveOverTime : Element
{
	float total_life = 0;
	float life = 0;
	GameObject my_object;
	Vector3 destination;
	Vector3 initial_position;
	bool local;

	public ElementMoveOverTime(float stunTicks, Vector3 dest, Vector3 initial, GameObject my_object, bool local)
	{ 
		this.total_life = stunTicks;
		this.my_object = my_object;
		this.destination = dest;
		this.initial_position = initial;
		this.local = local;
	}
	
	public override void update(float time_delta_fraction)
	{
		life += time_delta_fraction;
		float ratio = (float)life / total_life;

		if(local)
			my_object.transform.localPosition = Vector3.Lerp(initial_position, destination, ratio);
		else
			my_object.transform.position = Vector3.Lerp(initial_position, destination, ratio);

		if(ratio >= 1.0f)
			finished = true;
	}
}

public enum MovementType {ABSOLUTE, LOCAL, UIABSOLUTE, UILOCAL};

public class ElementMoveEase : Element
{
	float easeFactor = 0;
	GameObject my_object;
	Vector3 destination;
	Vector3 initial_position;
	MovementType movementType;

	public ElementMoveEase(Vector3 dest, float easeFactor, GameObject my_object, MovementType movementType)
	{ 
		this.my_object = my_object;
		this.destination = dest;
		this.easeFactor = easeFactor;
		this.movementType = movementType;
	}
	
	public override void update(float time_delta_fraction)
	{
		Vector3 diff = Vector3.zero;
		
		if(movementType == MovementType.LOCAL)
		{
			diff = (destination - my_object.transform.localPosition) * easeFactor;
			my_object.transform.localPosition += diff;
		}
		else if(movementType == MovementType.ABSOLUTE)
		{
			diff = (destination - my_object.transform.position) * easeFactor;
			my_object.transform.position += diff;
		} else if(movementType == MovementType.UIABSOLUTE)
		{
			Vector2 desiredPosition = new Vector2(destination.x, destination.y);
			Vector2 diff2 = (desiredPosition - my_object.GetComponent<RectTransform>().anchoredPosition) * easeFactor;
			my_object.GetComponent<RectTransform>().anchoredPosition += diff2;
			diff = new Vector3(diff2.x, diff2.y, 0);
		}

		if(diff.magnitude <= 0.01f)
			finished = true;
	}
}


public class ElementMoveRotateOverTime : Element
{
	int total_life = 0;
	int life = 0;
	GameObject my_object;
	Vector3 destination_pos;
	Vector3 initial_position;
	Quaternion initial_rot;
	Quaternion dest_rot;
	bool local;
	
	public ElementMoveRotateOverTime(int stunTicks, Vector3 dest_pos, Vector3 initial_pos, Vector3 dest_rot, Vector3 init_rot,
	                                 GameObject my_object, bool local)
	{ 
		this.total_life = stunTicks;
		this.my_object = my_object;
		this.destination_pos = dest_pos;
		this.initial_position = initial_pos;
		this.local = local;

		Quaternion q_init = new Quaternion();
		q_init.eulerAngles = init_rot;
		this.initial_rot = q_init;

		Quaternion q_dest = new Quaternion();
		q_dest.eulerAngles = dest_rot;
		this.dest_rot = q_dest;
	}
	
	public override void update(float time_delta_fraction)
	{
		life ++;
		float ratio = (float)life / total_life;
		
		if(local)
		{
			my_object.transform.localPosition = Vector3.Lerp(initial_position, destination_pos, ratio);
			my_object.transform.localRotation = Quaternion.Slerp(initial_rot, dest_rot, ratio);
		}
		else
		{
			my_object.transform.position = Vector3.Lerp(initial_position, destination_pos, ratio);
			my_object.transform.rotation = Quaternion.Slerp(initial_rot, dest_rot, ratio);
		}
		
		if(ratio >= 1.0f)
			finished = true;
	}
}

public class ElementWarpObject : Element
{
	GameObject my_object;
	Vector3 destination;
	Quaternion direction;
	MovementType movementType;

	public ElementWarpObject(Vector3 dest, Vector3 dir, GameObject my_object, MovementType movementType)
	{ 
		this.my_object = my_object;
		this.destination = dest;
		Quaternion q = new Quaternion();
		q.eulerAngles = dir;
		direction = q;
		this.movementType = movementType;
	}
	
	public override void onActive()
	{
		MonoBehaviour.print("WARP");

		if(movementType == MovementType.ABSOLUTE)
			my_object.transform.localPosition = destination;
		else if(movementType == MovementType.LOCAL)
			my_object.transform.position = destination;
		else if(movementType == MovementType.UIABSOLUTE)
			my_object.GetComponent<RectTransform>().anchoredPosition = destination;
		my_object.transform.rotation = direction;
		finished = true;
	}
}

public class ElementLoadScene : Element
{
	string scene_name;
	
	public ElementLoadScene(string scene_name)
	{ 
		this.scene_name = scene_name;
	}
	
	public override void onActive()
	{
		Application.LoadLevel(scene_name);
	}
}

public class ElementSetTextMesh : Element
{
	TextMesh my_mesh;
	string my_message;
	
	public ElementSetTextMesh(TextMesh t, string s)
	{ 
		this.my_mesh = t;
		this.my_message = s;
	}
	
	public override void onActive()
	{
		MonoBehaviour.print("TEXTMESH");
		my_mesh.text = my_message;
		finished = true;
	}
}

public class ElementOnOffCamera : Element
{
	Camera cam;
	bool on;
	
	public ElementOnOffCamera(Camera cam, bool on)
	{ 
		this.cam = cam;
		this.on = on;
	}
	
	public override void onActive ()
	{
		cam.enabled = on;
		finished = true;
	}
}

public class ElementInstantiateObject : Element
{
	GameObject prefab;
	Vector3 pos;
	Quaternion rot;
	
	public ElementInstantiateObject(GameObject prefab, Vector3 pos, Quaternion rot)
	{ 
		this.prefab = prefab;
		this.pos = pos;
		this.rot = rot;
	}
	
	public override void onActive ()
	{
		MonoBehaviour.Instantiate(prefab, pos, rot);
		finished = true;
	}
}

public class ElementExecuteAction : Element
{
	Action act;
	
	public ElementExecuteAction (Action act)
	{
		this.act = act;
	}
	
	public override void onActive()
	{
		act();
		finished = true;
	}
}

public class ElementFadeScreen : Element
{	
	GameObject fade_screen;
	float initial_fade = 0.0f;
	float fade_goal = 1.0f;
	float ease_factor = 0.1f;
	public ElementFadeScreen (GameObject fade_screen, float initial_fade, float fade_goal, float ease_factor)
	{
		this.fade_screen = fade_screen;
		this.initial_fade = initial_fade;
		this.fade_goal = fade_goal;
		this.ease_factor = ease_factor;
	}
	
	public override void onActive() 
	{
		Color current_color = fade_screen.GetComponent<Image>().color;
		fade_screen.GetComponent<Image>().color = new Color(current_color.r,
		                                                    current_color.g,
		                                                    current_color.b,
		                                                    initial_fade);
	}
	
	public override void update(float time_delta_fraction)
	{
		// BORDERS
		
		Color current_color = fade_screen.GetComponent<Image>().color;
		fade_screen.GetComponent<Image>().color = new Color(current_color.r,
		                                                    current_color.g,
		                                                    current_color.b,
		                                                    current_color.a + (fade_goal - current_color.a) * ease_factor * time_delta_fraction);
		
		if(fade_screen.GetComponent<Image>().color.a >= fade_goal - 0.02f)
		{
			fade_screen.GetComponent<Image>().color = new Color(current_color.r,
			                                                    current_color.g,
			                                                    current_color.b,
			                                                    fade_goal);
			finished = true;
		}
	}
}

public class ElementDialogue : Element
{
	string desired_text_content = "";
	string current_text_content = "";
	Text text_object;
	GameObject character_object;
	TextAnchor alignment;
	Sprite face_image;
	AudioClip text_clip;
	
	int max_character_timer = 0;
	int character_timer = 0;
	
	List<char> mildPauseCharacters = new List<char>(){ ',', '-', };
	List<char> longPauseCharacters = new List<char>(){ '.', '?', '!'};
	
	public ElementDialogue (string text_content, int max_character_timer, TextAnchor alignment, Text text_object, GameObject character_object, Sprite face_image, AudioClip text_clip)
	{
		this.text_object = text_object;
		this.alignment = alignment;
		this.desired_text_content = text_content;
		this.max_character_timer = max_character_timer;
		this.character_object = character_object;
		this.face_image = face_image;
	}
	
	public override void onActive()
	{	
		text_object.text = "";
		text_object.alignment = alignment;
		character_timer = max_character_timer;
		
		
		if(max_character_timer == -1)
		{
			text_object.text = desired_text_content;
			current_text_content = desired_text_content;
			finished = true;
		}
		else
		{
			if(character_object != null)
			{
				character_object.GetComponent<breath_effect>().rate = 3.0f;
				character_object.GetComponent<breath_effect>().amplitude = 0.05f;
			}
		}
	}
	
	public override void update(float time_delta_fraction)
	{
		character_timer --;
		if(character_timer <= 0)
		{
			character_timer = max_character_timer;
			if(current_text_content.Length < desired_text_content.Length)
			{
				char nextChar = desired_text_content[current_text_content.Length];
				current_text_content += nextChar;
				
				Camera.main.GetComponent<AudioSource>().PlayOneShot(text_clip, 1.0f);
				text_object.text = current_text_content;
				
				
				if(mildPauseCharacters.Contains(nextChar))
					character_timer += 10;
				else if(longPauseCharacters.Contains(nextChar))
					character_timer += 20;
			}
			else
			{
				finished = true;
			}
		}
	}
	
	public override void onRemove() 
	{
		if(character_object != null)
		{
			character_object.GetComponent<breath_effect>().rate = character_object.GetComponent<breath_effect>().initial_rate;
			character_object.GetComponent<breath_effect>().amplitude = character_object.GetComponent<breath_effect>().initial_amplitude;
		}
	}
}

/*
 * This element handles the 'chasing' behavior witnessed in enemies.
 */
public class ElementChase : Element
{
	float acceleration;
	
	Entity pursuer;
	Entity target;
	
	public ElementChase(Entity pursuer, Entity target, float acceleration)
	{
		this.pursuer = pursuer;
		this.target = target;
		this.acceleration = acceleration;
	}
	
	public override void update(float time_delta_fraction)
	{
		// Don't chase a stunned target.
		if(target.currentState == EntityState.STUNNED)
		{
			return;
		}
		
		Vector3 direction = (target.transform.position - pursuer.transform.position).normalized;
		pursuer.GetComponent<Rigidbody>().velocity += direction * acceleration;
	}
}

/*
 * This Element handles the Spin Attack behavior witnessed in enemies.
 */
public class ElementSpinAttack : Element
{
	float acceleration;
	
	Entity pursuer;
	Entity target;
	
	float timer = 360;
	
	public ElementSpinAttack(Entity pursuer, Entity target, float acceleration)
	{
		this.pursuer = pursuer;
		this.target = target;
		this.acceleration = acceleration;
	}
	
	public override void onActive()
	{
		pursuer.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
		
		// An entity performing a spin attack, may not be damaged.
		pursuer.currentState = EntityState.INVINCIBLE;
		pursuer.GetComponent<Rigidbody>().mass = 10;
		pursuer.GetComponent<Renderer>().material.color = Color.blue;
		pursuer.transform.localScale = Vector3.one * 1.25f;
	}
	
	public override void update(float time_delta_fraction)
	{
		timer -= time_delta_fraction;
		if(timer <= 0)
			finished = true;
		
		// Spin Like Crazy.
		pursuer.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 10000);
		
		// Don't chase a stunned target.
		if(target.currentState == EntityState.STUNNED)
		{
			return;
		}
		
		Vector3 direction = (target.transform.position - pursuer.transform.position).normalized;
		pursuer.GetComponent<Rigidbody>().velocity += direction * acceleration;
	}
	
	public override void onRemove()
	{
		pursuer.GetComponent<Rigidbody>().mass = 1;
		pursuer.currentState = EntityState.NORMAL;
		pursuer.GetComponent<Renderer>().material.color = Color.white;
		pursuer.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;
		pursuer.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
		
		Quaternion q = new Quaternion();
		q.eulerAngles = Vector3.zero;
		pursuer.transform.rotation = q;
		pursuer.transform.localScale = Vector3.one;
	}
}


