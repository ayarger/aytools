﻿using UnityEngine;
using System.Collections;

public class Utility {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static void SetLayerRecursively(GameObject go, string layerName)
	{
		int layerNumber = LayerMask.NameToLayer(layerName);
		foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
		{
			trans.gameObject.layer = layerNumber;
		}
	}

	public static void ColorObject(GameObject transformForSearch, Color color)
	{
		transformForSearch.GetComponent<Renderer>().material.color = color;
		foreach (Transform trans in transformForSearch.transform)
		{
			//Debug.Log (trans.name);
			ColorObject (trans.gameObject, color);
		}
	}

	public static void RecursivelyParentObject(GameObject transformForSearch, Transform newParent)
	{
		foreach (Transform trans in transformForSearch.transform)
		{
			//Debug.Log (trans.name);
			transformForSearch.transform.SetParent(newParent);
			RecursivelyParentObject (trans.gameObject, newParent);
		}
	}
}
