﻿using UnityEngine;
using System.Collections;

public class ConstantRotation : MonoBehaviour {

	public Vector3 rot = Vector3.zero;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(rot);
	}
}
