﻿using UnityEngine;
using System.Collections;

public class breath_effect : MonoBehaviour {

	public float sinCounter = 0;
	public float rate = 1.0f;
	public float initial_rate = 1.0f;
	public float amplitude = 0.002f;
	public float initial_amplitude = 0.002f;

	public float initial_vertical_scale = 1.0f;


	// Use this for initialization
	void Start () {
		initial_rate = rate;
		initial_amplitude = amplitude;
		initial_vertical_scale = transform.localScale.y;
	}
	
	// Update is called once per frame
	void Update () {
		sinCounter += 0.1f;
		transform.localScale = new Vector3(transform.localScale.x, initial_vertical_scale + Mathf.Abs(Mathf.Sin(sinCounter * rate) * amplitude), transform.localScale.z);
	}
}
