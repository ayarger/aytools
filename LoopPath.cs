﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoopPath : MonoBehaviour {

	public List<Vector3> nodes;

	public enum PathEnding { CONTINUATION, WARP, REVERSE, STOP };

	public PathEnding ending;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnDrawGizmos() {

		Gizmos.color = Color.green;
		//Gizmos.DrawMesh (gameObject.GetComponent<Mesh>(), transform.position + Vector3.up);

		for (int i = 0; i < nodes.Count; i++) {
			Vector3 node = nodes[i];
			Gizmos.DrawSphere (node, 0.5f);

			if(i < nodes.Count-1)
				Gizmos.DrawLine(node, nodes[i+1]);
			else
			{
				if(ending == PathEnding.CONTINUATION)
					Gizmos.DrawLine(node, nodes[0]);
			}
		}
	}
}
