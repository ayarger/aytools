﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {

	public int player_number = -1;
	public float running_speed = 4.0f;

	public List<Element> ele_queue = new List<Element>();

	public enum PlayerState { NORMAL, STUNNED, ATTACKING, KO, DEAD, INVENTORY, INTRO, GOAL };
	public PlayerState current_state = PlayerState.NORMAL;

	public static List<Player> instances = new List<Player>();

	// Use this for initialization
	void Start () {
		instances.Add(this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
