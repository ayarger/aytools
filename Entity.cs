﻿/*
 * Entity is a base class for "living" objects in the game.
 * 
 * "Living" objects have health, may be stunned, or made invincible.
 */

using UnityEngine;
using System.Collections;

public enum EntityState {NORMAL, STUNNED, INVINCIBLE};

public class Entity : MonoBehaviour {

	public int initialHealth = 3;
	public int health;

	public EntityState currentState = EntityState.NORMAL;

	// This function provides a way for Derived objects run logic during Awake(),
	// While reserving Awake() itself for this base Entity object.
	protected virtual void PostAwake() {}

	// Use this for initialization
	void Awake () {
		health = initialHealth;
		PostAwake();
	}
}